#!/bin/sh
cd /mnt/mtd/run/
cp RT2870STA_7601.dat /etc/Wireless/RT2870STA/
insmod /mnt/mtd/run/mt7601Usta.ko
ifconfig  wlan0 up

mdev -s
ifconfig wlan0 192.168.11.30 netmask 255.255.255.0 up
echo 1 > /proc/sys/vm/drop_caches

wpa_passphrase IPCAM ipcamera > /mnt/mtd/run/wpa.conf

wpa_supplicant -B -i wlan0 -Dwext -c /mnt/mtd/run/wpa.conf

iwpriv wlan0 connStatus